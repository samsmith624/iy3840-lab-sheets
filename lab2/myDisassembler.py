#!/usr/bin/python
import pefile	
from capstone import *

pe = pefile.PE("/home/kali/Desktop/malware_data_science/ch1/ircbot.exe") #loads the target PE file
entrypoint = pe.OPTIONAL_HEADER.AddressOfEntryPoint #gets the address of the entry point (relative to the image base address) from the "optional header"
entrypoint_address = entrypoint + pe.OPTIONAL_HEADER.ImageBase #calculates the absolute address for the entry point
binary_code = pe.get_memory_mapped_image()[entrypoint:entrypoint+100] #gets the binary code for the PE file, from the entrypoint we take 100 bytes. We can set larger or smaller sizes.
disassembler = Cs(CS_ARCH_X86, CS_MODE_32) #initializes the disassembler in order to disassemble x86 binary code, with 32 bit words
for instruction in disassembler.disasm(binary_code, entrypoint_address): #disassembles every instruction contained in binary_code, starting from entrypoint_address
    print(f"{instruction.mnemonic}\t{instruction.op_str}") #for each instruction, write the operation (instruction.mnemonic), for example "push", a tabulation character (\t), and the argument of the instruction (instruction.op_str), for example "0x2832c4"
