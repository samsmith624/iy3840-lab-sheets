#include <stdio.h>
#include <stdlib.h>

void normalFunc();
void evilFunc();

int main(int argc, char **argv) {
  int input_key = 0;
  int secret = 12349876;
  
  if(argc > 1) {
    input_key = atoi(argv[1]);
  }
  
  int secret_know = (input_key == secret) ? 1 : 0;
  
  if(secret_know)
    evilFunc();
  else
    normalFunc();
  return 0;
}

void normalFunc() {
  printf("Hello, world!\n");
}

void evilFunc() {
  printf("This string is nasty!\n");
}
