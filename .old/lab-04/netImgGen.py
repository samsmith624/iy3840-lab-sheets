#!/usr/bin/python

# Importing stuff we need
import pefile
import sys
import argparse
import os
import pprint
import logging
import networkx
import collections
import tempfile
from networkx.drawing.nx_agraph import write_dot
from networkx.algorithms import bipartite

# Using argparse to parse any command line arguments, just like before
args = argparse.ArgumentParser("Visualize shared image relationships between a directory of malware samples")
args.add_argument("--target_path",help="directory with malware samples")
args.add_argument("--output_file",help="file to write DOT file to")
args.add_argument("--malware_projection",help="file to write DOT file to")
args.add_argument("--resource_projection",help="file to write DOT file to")
args = args.parse_args() #arguments are now in the args variable
network = networkx.Graph() #creates a new, empty network

# Used to extract graphical assets from malware samples
class ExtractImages():
	# Constructor
	def __init__(self,target_binary):
		self.target_binary = target_binary
		self.image_basedir = None
		self.images = []
	# Function that extracts images from the binary
	def work(self):
		self.image_basedir = tempfile.mkdtemp() #generates a temporary directory
		# Within the temp directory, we set the path for other directories
		icondir = os.path.join(self.image_basedir,"icons") 
		bitmapdir = os.path.join(self.image_basedir,"bitmaps")
		raw_resources = os.path.join(self.image_basedir,"raw")
		for directory in [icondir,bitmapdir,raw_resources]:
			os.mkdir(directory) #generates such other directories. The os library contains instructions for the operative system, like if we were typing them in the terminal
		rawcmd = "wrestool -x {0} -o {1} 2> /dev/null".format(self.target_binary,raw_resources) #wrestool reads 16/32-bit Windows binaries, extracts the resources they contain, and move them to the "raw" directory we created earlier
		bmpcmd = "mv {0}/*.bmp {1} 2> /dev/null".format(raw_resources,bitmapdir) #moves found .bmp files from "raw" to "bitmap" directory
		icocmd = "icotool -x {0}/*.ico -o {1} 2> /dev/null".format(raw_resources,icondir) #icotool extracts icons and cursors (in this case, icons only since we have specified *.ico files) and moves them into the "icon" directory
		# 2> /dev/null suppresses output
		for cmd in [rawcmd,bmpcmd,icocmd]:
			try:
				os.system(cmd) #executes rawcmd on the first round, then bmpcmd and finally icocmd
			except Exception,msg: #exception catch
				pass #null operation, nothing happens
		for dirname in [icondir,bitmapdir]: #icondir and bitmapdir are now hopefully populated...
			for path in os.listdir(dirname): #...so for each of them we use listdir() that is equivalent to the 'ls' command, i.e. it lists all files and directories in the current path (since we populated these directories, we know that we will only find files
				logging.info(path) #on the root logger, the 'path' entry is added, with level=INFO (levels: DEBUG, INFO, WARNING, ERROR, CRITICAL)
				path = os.path.join(dirname,path)
				imagehash = hash(open(path).read()) #in python, hash values are integers used to quickly compare dictionary keys while looking up a dictionary
				#if the file ends with .png or .bmp, add it to the list of images we defined in the constructor
				if path.endswith(".png"):
					self.images.append((path,imagehash))
				if path.endswith(".bmp"):
					self.images.append((path,imagehash))
	def cleanup(self): #removes all temp directories
		os.system("rm -rf {0}".format(self.image_basedir))

# Search the target directory for PE files to extract images from
image_objects = []
for root,dirs,files in os.walk(args.target_path): #walk() scans directories and subdirectories
	for path in files:
		# Try to parse the path to see if it's a valid PE file
		try:
			pe = pefile.PE(os.path.join(root,path))
		except pefile.PEFormatError:
			continue #skips this iteration of the for cycle
		fullpath = os.path.join(root,path)
		# Now that we have our binary, let's extract its images, if any, by using our functions
		images = ExtractImages(fullpath)
		images.work()
		image_objects.append(images)
		# Create the network by linking malware samples to their images
		for path, image_hash in images.images:
			# Set the image attribute on the image nodes to tell GraphViz to render images within these nodes
			if not image_hash in network: #if the hash hasn't appeared in the network yet, this is a new image that hasn't been added to the network, hence we do so
				network.add_node(image_hash,image=path,label='',type='image')
			node_name = path.split("/")[-1] #splits the path with "/" as separator and gets the last element of the list
			network.add_node(node_name,type="malware")
			network.add_edge(node_name,image_hash)

# Write the bipartite network, then do the two projections and write them, just like in the previous activity
write_dot(network, args.output_file) #neato should be slightly better to visualize this
malware = set(n for n,d in network.nodes(data=True) if d['type']=='malware')
resource = set(network) - malware
malware_network = bipartite.projected_graph(network, malware)
resource_network = bipartite.projected_graph(network, resource)
write_dot(malware_network,args.malware_projection) #everything will do to visualize this
write_dot(resource_network,args.resource_projection) #fdp or neato should be better for this