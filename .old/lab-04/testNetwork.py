#!/usr/bin/python
import networkx
from networkx.drawing.nx_agraph import write_dot #this will allow us to save our network in a .dot file

network = networkx.Graph() #instantiates a network with no nodes and no edges
nodes = ["hello", "world", 1, 2, 3] #we create a list of nodes...
for node in nodes:
	network.add_node(node)#...and we add them to our network. However, they are not linked yet!
network.add_edge("hello","world") #links the node 'hello' to the node 'world'

# We can give lots of attributes to our nodes and edges, when we define them or even after a while! Some examples:
network.node[1]["someAttribute"] = "attrValue" #generic attribute, defined after creation of the node. This won't get plotted
network.add_edge(1, 2, penwidth=5) #width of the line connecting the nodes, defined during the creation of the edge
network[1][2]["color"]="red" #color of the line, defined after the creation of the edge
network.node["hello"]["color"]="green" #color of the node, defined after the creation of the node
network.node[3]["shape"]="diamond" #shape of the node, defined after the creation of the node
network.add_node(4, shape="egg") #shape of the node, defined during the creation of the node
network.node[2]["label"] = "This is a label for Node 2" #defined after creation of the node
network["hello"]["world"]["label"] = "This is a label for an Edge" #defined after creation of the edge
network.add_edge("world",4,label="Another label") #defined during the creation of the edge

# That should be enough. Let's export our test network:
write_dot(network, "test.dot")