#!/usr/bin/python

import sys
import pefile #we use this to parse PE binaries
import argparse
import os
import pprint
import networkx
import re
from networkx.drawing.nx_agraph import write_dot
import collections
from networkx.algorithms import bipartite

# Let's define what parameters are accepted by our program (see "help=" field for info)
args = argparse.ArgumentParser("Visualize shared hostnames between a directory of malware samples")
args.add_argument("--target_path",help="directory with malware samples")
args.add_argument("--output_file",help="file to write DOT file to")
args.add_argument("--malware_projection",help="file to write DOT file to")
args.add_argument("--hostname_projection",help="file to write DOT file to")
args = args.parse_args() #stores arguments into the args variable
network = networkx.Graph() #creates a new graph/network

valid_hostname_suffixes = map(lambda string: string.strip(), open("/home/osboxes/malware_data_science/ch4/code/domain_suffixes.txt")) #loads valid domain extensions from this file
valid_hostname_suffixes = set(valid_hostname_suffixes) #the set() method takes as input an iterable sequence and returns a sorted iterable sequence with no recurring elements
# Function that tries to find hostnames via regular expression
def find_hostnames(string):
	possible_hostnames = re.findall(r'(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-]{,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}', string) #regular expression that matches hostnames: strings that match this regex begin with a letter or a number and can contain dashes (although not at the beginning and not right before the dot that introduces the domain extension). The extension can contain characters only and must be between 2 and 6 characters long.
	valid_hostnames = filter(
		lambda hostname: hostname.split(".")[-1].lower() \
		in valid_hostname_suffixes,
		possible_hostnames
	) #valid_hostnames will contain the hostnames (converted to lowercase) that also have a valid suffix. A suffix is valid if it is listed in the file we imported above
	return valid_hostnames

# Search the target directory for valid Windows PE executable files
for root,dirs,files in os.walk(args.target_path):
	for path in files:
	# Try opening the file with pefile to see if it's really a PE file
		try:
			pe = pefile.PE(os.path.join(root,path))
		except pefile.PEFormatError: #exception catch
			continue #jumps to the next item in the for cycle
		fullpath = os.path.join(root,path)
		# Extract printable strings from the target sample
		strings = os.popen("strings '{0}'".format(fullpath)).read()
		# Let's use the search_doc function in the included reg module to find hostnames
		hostnames = find_hostnames(strings)
		if len(hostnames): #if there is at least one element in hostnames, this is true
			# Add the nodes and edges for the bipartite network
			network.add_node(path,label=path[:32],color='black',penwidth=5,bipartite=0) #adds a node with the first 32 chars as label, and that is part of the group 0 in the bipartite network
		for hostname in hostnames:
			network.add_node(hostname,label=hostname,color='blue',penwidth=10,bipartite=1) #for each hostname found in "path" (see 'for path in files') add another node in the group 1 of the bipartite network...
			network.add_edge(hostname,path,penwidth=2) #...and link it to the path node in the group 0
		if hostnames: #print results
			print "Extracted hostnames from:",path
			pprint.pprint(hostnames)

# After we have built the network, let's write the dot file to disk. fdp should give the best output for this
write_dot(network, args.output_file)
# Let's now divide our network in two groups to show their relationships
malware = set(n for n,d in network.nodes(data=True) if d['bipartite']==0) #gets all malware, i.e. everything in the group 0
hostname = set(network)-malware #gets the remainder (hostnames)

# use NetworkX's bipartite network projection function to produce the malware and hostname projections
malware_network = bipartite.projected_graph(network, malware)
hostname_network = bipartite.projected_graph(network, hostname)

# write the projected networks to disk as specified by the user
write_dot(malware_network,args.malware_projection) #fdp, sfdp and neato all give rather equivalent results
write_dot(hostname_network,args.hostname_projection) #neato should give the better representation