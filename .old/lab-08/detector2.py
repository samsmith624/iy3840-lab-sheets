#!/usr/bin/python

# Importing stuff we need
import os
import sys
import pickle
import argparse
import re
import numpy
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction import FeatureHasher
from sklearn import metrics
from matplotlib import pyplot
from numpy import random

# We define a function that will extract the strings by using regular expressions
def get_string_features(path,hasher):
	chars = r" -~" #r is for "raw", meaning you don't have to escape chars. " -~" is the set of characters that goes from %20 (whitespace) to %7E (tilde).
	min_length = 5
	string_regexp = '[%s]{%d,}' % (chars, min_length) #this regex will match every sequence of characters included in the set above, that is at least 5 chars long
	file_object = open(path)
	data = file_object.read()
	pattern = re.compile(string_regexp) #compile() transforms the regex string in objects that can be used for pattern matching
	strings = pattern.findall(data) #returns an array that includes all strings that match our regex

	# We want to store string features in dictionary form...
	string_features = {}
	for string in strings:
		string_features[string] = 1 #...so for each string found, an entry in string_features is created. For example, if we had a "foobar" string, we would have that string_features[foobar]=1.
	hashed_features = hasher.transform([string_features]) #hashing trick
	# Unfortunately, features are returned as a list of arrays, and in a sparse format nonetheless.
	# We then need to get them back into a dense numpy vector.
	hashed_features = hashed_features.todense() #returns a dense matrix
	hashed_features = numpy.asarray(hashed_features) #returns a numpy vector
	hashed_features = hashed_features[0] #returns the actual feature vector
	# The following lines print some info and return the array
	print "Extracted {0} strings from {1}".format(len(string_features),path)
	return hashed_features

# We still need to train our detector! But first, let's write a function to build our training model.
def get_training_data(benign_path,malicious_path,hasher):
	# This function will help us get absolute file paths for a given directory
	def get_training_paths(directory):
		targets = []
		for path in os.listdir(directory):
			targets.append(os.path.join(directory,path))
		return targets
	malicious_paths = get_training_paths(malicious_path) #gets absolute file paths for malicious training samples
	benign_paths = get_training_paths(benign_path) #gets absolute file paths for benign training samples
	X = [get_string_features(path,hasher) 
            for path in malicious_paths + benign_paths] #do you recall get_string_features()? Yes, we defined it earlier! For each malicious samples, it gets all string features that match our criteria
	y = [1 for i in range(len(malicious_paths))] + [0 for i in range(len(benign_paths))] #if the sample was labeled as malicious, assign 1, otherwise 0. Remember that we already know if these samples are malicious or not. This is, in other words, our ground truth.
	return X, y #returns our training model

# We can now train our detector using the training model we generated!
def train_detector(X,y,hasher):
	classifier = RandomForestClassifier() #sets the classifier as a decision tree that improves predictive accuracy and controls over-fitting
	classifier.fit(X,y) #trains the detector
	pickle.dump((classifier,hasher),open("saved_detector.pkl","w+")) #we save the detector in the saved_detector.pkl file, for future use

# With our new detector, we can try to detect malware in new unknown binaries. Let's write a function that does it.
def scan_file(path):
	if not os.path.exists("saved_detector.pkl"): #we look for our saved detector, and exit if we don't find it
		print "Train a detector before scanning files."
		sys.exit(1)
	with open("saved_detector.pkl") as saved_detector:
		classifier, hasher = pickle.load(saved_detector) #if the file is found, classifier and hasher are loaded from the pickle file
	features = get_string_features(path,hasher) #we extract string features from the unknown binary
	result_proba = classifier.predict_proba(features)[0][1] #the classifier tries to infer whether the binary is malicious or not, given the extracted features. The second value of the first element (i.e. [0][1]) from the returned array corresponds to such probability
	if result_proba > 0.5:
		print "It appears this file is malicious! Estimated accuracy: " + str(result_proba*100) + "%. Original result_proba value: " + str(result_proba)
	else:
		print "It appears this file is benign. Estimated accuracy: " + str((1-result_proba)*100) + "%. Original result_proba value: " + str(result_proba)
		
### To test accuracy of our detector, we have to split our data in two groups: the training set and the test set. We will do this randomly.
def evaluate(X,y,hasher):
	X, y = numpy.array(X), numpy.array(y) #conversion of X and y into numpy arrays
	indices = range(len(y)) #range() generates the list of integer numbers from 0 to the specified parameter, in this case the length of y
	random.shuffle(indices) #numbers in the indices list are shuffled
	X, y = X[indices], y[indices] #elements in X and y are shuffled, following the sequence of numbers in the shuffled vector "indices". Please note that X and y belonging to the same sample are not separated. We could say that we shuffle the (X,y) pairs, that however keep their association
	splitpoint = len(X) * 0.5 #calculates the pivot index
	splitpoint = int(splitpoint) #if the number calculated above is not an integer, it gets always rounded down
	training_X, test_X = X[:splitpoint], X[splitpoint:] #the training set will be made by the elements before the pivot. The test set will be the remainder
	training_y, test_y = y[:splitpoint], y[splitpoint:] #same as the line above, but for y
	# We then train the model with the training set only. This is basically train_detector() without the pickle save file	
	classifier = RandomForestClassifier()
	classifier.fit(training_X,training_y)
	scores = classifier.predict_proba(test_X)[:,-1] #tries to predict maliciousness of test samples
	# For Section 5.4 of the lab, delete from here
	# Let's now visualize our ROC curve in order to have a glance at our detector's accuracy
	fpr, tpr, thresholds = metrics.roc_curve(test_y, scores) #this function returns some triplets made by: fpr = false positive ratio, tpr = true positive ratio, decision thresholds - the latter however won't be plotted.
	pyplot.plot(fpr,tpr,'r-') #plots fpr and tpr with a red line
	pyplot.xlabel("Detector false positive rate") #label of X axis
	pyplot.ylabel("Detector true positive rate") #label of Y axis
	pyplot.title("Detector ROC Curve") 
	pyplot.show()
	# For Section 5.4 of the lab, delete up to this point, then uncomment the following:
	
# 	precision, recall, thresholds = metrics.precision_recall_curve(test_y, scores) #function that calculates precision-recall values
# 	norm_scores = scores
# 	for s in range(len(norm_scores)): #the f1_score() and confusion_matrix() functions called below can't work with continuous and binary values at the same time, so we are converting every value within "scores" in a binary value, and we are saving these new values in "norm_scores"
# 		if norm_scores[s] > 0.5:
# 			norm_scores[s] = 1
# 		else:
# 			norm_scores[s] = 0
# 	f1 = metrics.f1_score(test_y, norm_scores, average='binary') #function that calculates the F1 score
# 	print ("F1 Score: " + str(f1))
# 	confusion = metrics.confusion_matrix(test_y, norm_scores) #function that calculates the confusion matrix
# 	print ("Confusion matrix:")
# 	print (confusion)
# 	pyplot.plot(recall,precision,'r-')
# 	pyplot.title("Precision-Recall Curve") 
# 	pyplot.xlabel("Recall")
# 	pyplot.ylabel("Precision")
# 	pyplot.grid()
# 	pyplot.show()

### This function will do K-Fold Cross-Validation, with K = 3
def cv_evaluate(X,y,hasher):
	from sklearn.cross_validation import KFold #imports the kfold library that is present in sklearn
	X, y = numpy.array(X), numpy.array(y)
	fold_counter = 0
	for train, test in KFold(len(X), 3, shuffle=True): #we instantiate the KFold class, with the following parameters, in order: length of our set; number of folds (K); boolean value to indicate if we want to randomly shuffle the set before folding it or not
		training_X, training_y = X[train], y[train] #"train" contains all the indexes of the training samples
		test_X, test_y = X[test], y[test] #"test" contains all the indexes of the testing samples
		classifier = RandomForestClassifier()
		classifier.fit(training_X,training_y)
		scores = classifier.predict_proba(test_X)[:,-1]
		fpr, tpr, thresholds = metrics.roc_curve(test_y, scores)
		pyplot.semilogx(fpr,tpr,label="Fold number {0}".format(fold_counter)) #adds legend to match fold number to color of the line
		fold_counter += 1 #we increment the fold_counter, which is here only to make the legend above consistent
	pyplot.xlabel("Detector false positive rate")
	pyplot.ylabel("Detector true positive rate")
	pyplot.title("Detector Cross-Validation ROC Curves")
	pyplot.legend()
	pyplot.grid()
	pyplot.show()

# We defined all functions we need, however we still have to write our core program that calls them.
parser = argparse.ArgumentParser("get windows object vectors for files")
# Our python program will ask for three parameters upon execution (see "help=" field):
parser.add_argument("--malware_paths",default=None,help="Path to malware training files")
parser.add_argument("--benignware_paths",default=None,help="Path to benignware training files")
parser.add_argument("--scan_file_path",default=None,help="File to scan")
parser.add_argument("--evaluate",default=False,action="store_true",help="Perform cross-validation")###Adding this parameter to evaluate the detector
args = parser.parse_args() #parses arguments into the args variable
hasher = FeatureHasher(20000) #our hasher will include 20000 hashed features when performing the hashing trick
if args.malware_paths and args.benignware_paths and args.evaluate: ###checks if paths have been specified and if we want to evaluate
	X, y = get_training_data(args.benignware_paths,args.malware_paths,hasher)
	evaluate(X,y,hasher)
if args.malware_paths and args.benignware_paths: #checks if paths for both malware and benignware training samples have been specified
	X, y = get_training_data(args.benignware_paths,args.malware_paths,hasher)
	train_detector(X,y,hasher)
elif args.scan_file_path: #if benignware and malware paths have NOT been specified, checks if path for the unknown binary has been specified
	scan_file(args.scan_file_path)
else:
	print "[*] You did not specify a path to scan," \
	" nor did you specify paths to malicious and benign training files" \
	" please specify one of these to use the detector.\n"
	parser.print_help()
