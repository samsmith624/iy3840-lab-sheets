
#!/usr/bin/python

import argparse
import os
import murmur #hashing library used to compute minhashes
import shelve #simple database module, we will use this to store minhashes
import numpy as np
from easyJaccard import * #we need some functions we declared in the previous section, so we will import easyJaccard to use them

NUM_MINHASHES = 256
SKETCH_RATIO = 8

def wipe_database():
	"""
	This function uses the python standard library 'shelve' database to persist
	information, storing the database in the file 'samples.db' in the same
	directory as the actual Python script. 'wipe_database' deletes this file
	effectively resetting the database.
	"""
	dbpath = "/".join(__file__.split('/')[:-1] + ['samples.db']) #gets the path of the samples.db file in the same directory of the script
	os.system("rm -f {0}".format(dbpath)) #deletes the file.

def get_database():
	"""
	Helper function to retrieve the 'shelve' database, which is a simple
	key value store.
	"""
	dbpath = "/".join(__file__.split('/')[:-1] + ['samples.db'])
	return shelve.open(dbpath,protocol=2,writeback=True) #opens the database. The protocol variable allows to define the pickle version to be used, whilst writeback=true caches all accessed elements. Please note that if we access lots of elements, this can actually slow down our program instead of accelerating it.

def minhash(features):
	"""
	This is where the minhash magic happens, computing both the minhashes of
	a sample's features and the sketches of those minhashes. The number of
	minhashes and sketches computed is controlled by the NUM_MINHASHES and
	NUM_SKETCHES global variables declared at the top of the script.
	"""
	minhashes = []
	sketches = []
	for i in range(NUM_MINHASHES): #range() returns a list of numbers from 0 to NUM_MINHASHES-1
		minhashes.append(min([murmur.string_hash(`feature`,i) for feature in features])) #for each feature, calculates the hash using i as a seed, and adds only the min hash to the minhashes array
	for i in xrange(0,NUM_MINHASHES,SKETCH_RATIO): #xrange() returns the generator object for the list of numbers, that will display numbers via lazy evaluation
		sketch = murmur.string_hash(`minhashes[i:i+SKETCH_RATIO]`) #builds a sketch by hashing together all minhashes from i to i+SKETCH_RATIO,
		sketches.append(sketch) #adds such sketch to the array
	return np.array(minhashes),sketches

def store_sample(path):
	"""
	Function that stores a sample and its minhashes and sketches in the
	'shelve' database
	"""
	db = get_database() #we defined this earlier, it gets the database and stores it into the db variable
	features = getstrings(path) #defined in the previous section of this lab, path variable includes filename
	minhashes,sketches = minhash(features) #we defined this earlier, it returns minhashes and sketches, given features of one sample
	for sketch in sketches:
		sketch = str(sketch) #turns each sketch into a string
		if not sketch in db: #looks for this sketch in the database. If it is not found...
			db[sketch] = set([path]) #...adds an entry with the path to the file
		else:
			obj = db[sketch] #otherwise, if this sketch already exists, then there is another file that has the same sketch...
			obj.add(path) #hence, we add this file to the list of files which share this sketch
			db[sketch] = obj
		db[path] = {'minhashes':minhashes,'comments':[]} #stores minhashes for this file
		db.sync() #saves edits to the database
	print "Extracted {0} features from {1} ...".format(len(features),path)

def comment_sample(path):
	"""
	Function that allows a user to comment on a sample.  The comment the
	user provides shows up whenever this sample is seen in a list of similar
	samples to some new samples, allowing the user to reuse their
	knowledge about their malware database.
	"""
	db = get_database()
	comment = raw_input("Enter your comment:") #raw_input() prompts the user to type some text that will be stored in the "comment" variable. Please note that raw_input() doesn't work in Python 3, where you have to use input(). We cannot use this function since in Python 2 it has a different behaviour.
	if not path in db:
		store_sample(path) #if this file isn't found within the database, we create one entry for it
	comments = db[path]['comments'] #we get current comments for this file...
	comments.append(comment) #...and we append the new ones
	db[path]['comments'] = comments #then we edit the database entry...
	db.sync() #...and save our edits
	print "Stored comment:", comment

def search_sample(path):
	"""
	Function that searches for samples similar to the sample provided by the
	'path' argument, listing their comments, filenames, and similarity values
	"""
	db = get_database()
	features = getstrings(path) #we get features for our sample...
	minhashes, sketches = minhash(features) #...and then calculate their minhashes and sketches
	neighbors = []
	for sketch in sketches:
		sketch = str(sketch)
		if not sketch in db: #we look, for every sketch, if it is in the database or not
			continue #if it's not found, go to the next sketch
		for neighbor_path in db[sketch]: #if it's found, for each file
			neighbor_minhashes = db[neighbor_path]['minhashes'] #1. we get minhashes
			similarity = (neighbor_minhashes == minhashes).sum() / float(NUM_MINHASHES) #2. we calculate similarity as the sum of the matching minhashes divided by the number of minhashes for each sample 
			neighbors.append((neighbor_path, similarity)) #we add this file to the neighbor array we defined earlier, together with its similarity score
	neighbors = list(set(neighbors)) #we turn the neighbor array into a list
	neighbors.sort(key=lambda entry:entry[1], reverse=True) #sorts the neighbor list by the second parameter (similarity), in descending order (from the highest to the lowest similarity)
	# We now output our findings
	print ""
	print "Sample name".ljust(64), "Shared code estimate" #ljust "left justifies" the "Sample name" string using 64 characters, using "space" as filler character
	for neighbor, similarity in neighbors:
		short_neighbor = neighbor.split("/")[-1] #splits neighbor by the "/" character (since it's a path) and gets the last string, i.e. the filename
		comments = db[neighbor]['comments']
		print str("[*] "+short_neighbor).ljust(64), similarity
		for comment in comments:
			print "\t[comment]",comment

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description="""
	Simple code-sharing search system which allows you to build up
	a database of malware samples (indexed by file paths) and
	then search for similar samples given some new sample
	""")
	parser.add_argument("-l", "--load", dest="load", default=None, help="Path to malware directory or file to store in database")
	parser.add_argument("-s", "--search", dest="search", default=None, help="Individual malware file to perform similarity search on")
	parser.add_argument("-c", "--comment", dest="comment", default=None, help="Comment on a malware sample path")
	parser.add_argument("-w", "--wipe", action="store_true", default=False, help="Wipe sample database")
	args = parser.parse_args() #stores arguments into the args variable
	if args.load: #if "--load" or "-l" was used, we store into the database all features of the files found in the given directory
		malware_paths = [] 
		malware_features = dict() #dictionary form
		for root, dirs, paths in os.walk(args.load):
			# Let's walk the target directory tree and store all of the file paths
			for path in paths:
				full_path = os.path.join(root,path)
				malware_paths.append(full_path)
		malware_paths = filter(pecheck, malware_paths) #filters out any paths that aren't PE files
		# Get and store in the database all the strings for all of the malware PE files
		for path in malware_paths:
			store_sample(path)
	if args.search: #if "--search" or "-s" was used, we extract features, minhashes and sketches for the target file and look for similarities within our database
		search_sample(args.search) #we defined this earlier
	if args.comment: #if "--comment" or "-c" was used, we can add a comment to a file
		comment_sample(args.comment) #we defined this earlier
	if args.wipe: #only use "--wipe" or "-w" when you want to wipe the database!
		wipe_database() #we defined this earlier