#!/usr/bin/python

#Plotting ransomware detection rates
import pandas
from matplotlib import pyplot
import dateutil #useful library for parsing of dates

malware = pandas.read_csv("/home/osboxes/malware_data_science/ch9/code/malware_data.csv")
malware['fs_date'] = [dateutil.parser.parse(d) for d in malware['fs_bucket']] #parses every date in the 'fs_bucket' field into a new field, 'fs_date'
ransomware = malware[malware['type'] == 'ransomware'] #filters malware so that we only select ransomware
pyplot.plot(ransomware['fs_date'], ransomware['positives'], 'ro', alpha=0.05) #performs the plot. On X, dates. On Y, positives. Plotted using red circles. 5% alpha. 
pyplot.title("Ransomware Detections Over Time")
pyplot.xlabel("Date")
pyplot.ylabel("Number of antivirus engine detections")
pyplot.show()
