#!/usr/bin/python

#Plotting the distribution of malware file sizes versus positive detections
import pandas
from matplotlib import pyplot
import seaborn
import numpy

malware = pandas.read_csv("/home/osboxes/malware_data_science/ch9/code/malware_data.csv")
axis=seaborn.jointplot(x=numpy.log10(malware['size']), y=malware['positives'], kind="kde") #jointplot has different "modes", identified by the "kind" variable. The KDE (Kernel Density Estimate) is basically a heatmap. The result is similar to plot3.py but it's both more precise and appealing. 
axis.set_axis_labels("Bytes in malware file (log base-10)", "Number of engines detecting malware (out of 57)") #please note the different command to assign labels
pyplot.show() #this could take a bit
