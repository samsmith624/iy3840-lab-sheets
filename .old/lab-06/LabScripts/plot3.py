#!/usr/bin/python

#Plotting ransomware and worm detection rates
import pandas
from matplotlib import pyplot
import dateutil

malware = pandas.read_csv("/home/osboxes/malware_data_science/ch9/code/malware_data.csv")
malware['fs_date'] = [dateutil.parser.parse(d) for d in malware['fs_bucket']]
ransomware = malware[malware['type'] == 'ransomware'] 
worms = malware[malware['type'] == 'worm'] #filter to select worms only

# We will now actually build 2 plots and then merge them into a single one.
pyplot.plot(ransomware['fs_date'], ransomware['positives'], 'ro', label="Ransomware", markersize=3, alpha=0.05) #plots with red circles of size 3 that will be labelled as "Ransomware". 5% alpha.
pyplot.plot(worms['fs_date'], worms['positives'], 'bo', label="Worm", markersize=3, alpha=0.05) #plots with blue circles of size 3 that will belabelled as "Worm". 5% alpha.
pyplot.legend(framealpha=1, markerscale=3.0) #legend won't be transparent (100% alpha) and markers will have size 3
pyplot.xlabel("Date")
pyplot.ylabel("Number of detections")
pyplot.ylim([0, 57])
pyplot.title("Ransomware and Worm Vendor Detections Over Time")
pyplot.show() #shows both plots merged! You can do this with even more than two plots.
