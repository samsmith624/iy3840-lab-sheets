#!/usr/bin/python

#Plotting the distribution of antivirus detections
import pandas
from matplotlib import pyplot
import seaborn

malware = pandas.read_csv("/home/osboxes/malware_data_science/ch9/code/malware_data.csv")
axis = seaborn.distplot(malware['positives']) #a distribution plot for the 'positives' field. Similar to the count plot we have seen earlier
axis.set(xlabel="Number of engines detecting each sample (out of 57)", ylabel="Amount of samples in the dataset", title="Commercial Antivirus Detections for Malware")
pyplot.show()
