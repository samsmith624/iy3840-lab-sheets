#!/usr/bin/python

#Violin plot of file sizes by malware type
import pandas
from matplotlib import pyplot
import seaborn

malware = pandas.read_csv("/home/osboxes/malware_data_science/ch9/code/malware_data.csv")
axis = seaborn.violinplot(x=malware['type'], y=malware['size']) #violin plot
axis.set(xlabel="Malware type", ylabel="File size in bytes (log base-10)", title="File Sizes by Malware Type", yscale="log")
pyplot.show()
