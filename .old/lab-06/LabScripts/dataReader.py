#!/usr/bin/python

import pandas #data analysis library

malware = pandas.read_csv("/home/osboxes/malware_data_science/ch9/code/malware_data.csv") #our data in .csv will become a "DataFrame", a format which is easy to analyze, transform, query, etc.
print(malware) #let's visualize the content of our .csv file, i.e. our dataset, in the DataFrame format

# Pandas allows us to manipulate our dataset to get useful information.
print("\nOutput of describe() function:")
print(str(malware.describe()) + "\n") #describe() returns some statistics about our dataset

# We can "query" our DataFrame object to extract even more information. Some examples:
print("Maximum values for all fields: \n" + str(malware.max()) + "\n")
print("Mean value for one field ('positives', in this case): \n" + str(malware['positives'].mean()))

# Lastly, we can even "query" DataFrame objects using conditions:
print("Mean of 'positives' field for worm 'type': \n"+str(malware[malware['type'] == 'worm']['positives'].mean()))
print("Mean of 'positives' field for entries with 'size' higher than 3M: \n" + str(malware[malware['size'] > 3000000]['positives'].mean()))
