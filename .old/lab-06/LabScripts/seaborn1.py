#!/usr/bin/python

#Plotting number of examples of each malware type we have in our dataset
import pandas
from matplotlib import pyplot
import seaborn #a library to build more complex plots, also visually more appealing

malware = pandas.read_csv("/home/osboxes/malware_data_science/ch9/code/malware_data.csv")
seaborn.countplot(x='type', data=malware) #performs a "count" barplot, with the classic vertical bars. This plot will contain the number of entries for each 'type' in our dataset
pyplot.show() #just like before, this displays the plot
