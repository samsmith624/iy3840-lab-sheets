#!/usr/bin/python

#Plotting the relationship between malware size and vendor detections
import pandas
from matplotlib import pyplot #we will use pyplot to plot our data

malware = pandas.read_csv("/home/osboxes/malware_data_science/ch9/code/malware_data.csv")
pyplot.plot(malware['size'], malware['positives'], 'bo', alpha=0.01) #performs the plot. On the x axis, the 'size' field. On the y axis, 'positives'. b stands for blue and o for circles, so this data will be plotted with blue circles. Alpha is the transparency (1% in this case: the higher this value, the more the item is visible. This means that with 0.01, a single circle on the plot will be barely visible). For other shapes/colors, visit http://matplotlib.org
pyplot.xscale("log") #values in the x axis will be scaled using base 10 log
pyplot.ylim([0,57]) #lower limit for y axis is 0, upper limit is 57
pyplot.xlabel("File size in bytes (log base-10)") #label for x axis
pyplot.ylabel("Number of detections") #label for y axis
pyplot.title("Number of Antivirus Detections Versus File Size") #title for the plot
pyplot.show() #outputs the image
