#!/bin/bash

mkdir Lab10_Installs
cd Lab10_Installs

#Install java
wget "https://github.com/adoptium/temurin11-binaries/releases/download/jdk-11.0.17%2B8/OpenJDK11U-jdk_x64_linux_hotspot_11.0.17_8.tar.gz"
tar xvzf OpenJDK11U-jdk_x64_linux_hotspot_11.0.17_8.tar.gz
BINS=$(echo "$(pwd)/jdk-11.0.17+8/bin/*")

for F in $BINS
do
        #echo "/usr/bin/$(basename -- $F)"
        LINK=$(echo "/usr/bin/$(basename -- $F)")
        if [ ! -e "$LINK" ]
        then
                sudo ln -s "$F" "$LINK"
        fi
done    

#Install jadx
wget "https://github.com/skylot/jadx/archive/refs/heads/master.zip"
unzip master.zip
cd jadx-master
./gradlew dist
BINS=$(echo "$(pwd)/build/jadx/bin/")
sudo ln -s "$(echo "$BINS")jadx" "/usr/bin/jadx"
sudo ln -s "$(echo "$BINS")jadx-gui" "/usr/bin/jadx-gui"
cd ..

cd ..
