#!/usr/bin/python

# Importing stuff we need
from sklearn import tree #decision tree module
from sklearn.feature_extraction import DictVectorizer #translates readable training data in "DICTionary form" to the Vector representation

# From those imported libraries, we instantiate two classes
classifier = tree.DecisionTreeClassifier() #our detector, with sklearn's default decision tree settings
vectorizer = DictVectorizer(sparse=False) #sparse vectors save memory but are hard to use, hence we set this value to 'false'

# Initializing eight training samples with two features each, "packed" and "contains_encrypted"
training_examples = [
{'packed':1,'contains_encrypted':0},
{'packed':0,'contains_encrypted':0},
{'packed':1,'contains_encrypted':1},
{'packed':1,'contains_encrypted':0},
{'packed':0,'contains_encrypted':1},
{'packed':1,'contains_encrypted':0},
{'packed':0,'contains_encrypted':0},
{'packed':0,'contains_encrypted':0},
]

# Initializing label vector for each example. 0 is benign, 1 is malicious
ground_truth = [1,1,1,1,0,0,0,0] #this means that the first sample in the training set is malicious, the second is malicious as well, etc.

# Training the classifier
vectorizer.fit(training_examples) #initializes vectorizer with the training data
X = vectorizer.transform(training_examples) #transforms training_examples to vector form
y = ground_truth #just by convention, not really needed
classifier.fit(X,y) #actually trains the classifier

# Now that we have trained our classifier, let's test it with a new sample
test_example = {'packed':1,'contains_encrypted':0} #our test example will be packed but won't contain encrypted data
test_vector = vectorizer.transform(test_example) 
print classifier.predict(test_vector) #tries to predict whether the sample is malicious or not and prints the result.
