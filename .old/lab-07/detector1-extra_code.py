with open("classifier.dot","w") as output_file: #opens (or creates, if it does not exist), the classifier.dot file to Write content in it.
	tree.export_graphviz(
		classifier,
		feature_names=vectorizer.get_feature_names(),
		out_file=output_file	
	) #sklearn method that exports a decision tree in DOT format

import os
#executes this command in Ubuntu to convert the file then open the image
os.system("dot classifier.dot -Tpng -o classifier.png; xdg-open classifier.png")
