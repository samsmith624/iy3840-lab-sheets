from sklearn import metrics

validation_labels = validation_data[1] #array containing the real benign/malicious label
validation_scores = [el[0] for el in model.predict(validation_data[0])] #array containing our model's predictions
fpr, tpr, thres = metrics.roc_curve(y_true=validation_labels, y_score=validation_scores) #this function computes the roc curve and returns false positive rates, true positive rates and thresholds on the decision function used to compute fpr and tpr
auc = metrics.auc(fpr, tpr) #computes area under the curve
print('Validation AUC = {}'.format(auc))
