#batch_size is how many samples we want to return for training, for each cycle.
def my_generator(benign_files, malicious_files, path_to_benign_files, path_to_malicious_files, batch_size, features_length=1024):
    n_samples_per_class = batch_size / 2 #we want our batch samples to be evenly distributed, half benign and half malicious
    assert len(benign_files) >= n_samples_per_class #will halt the program raising an AssertionError exception if condition is not true. This will prevent the program from running if we do not have enough data.
    assert len(malicious_files) >= n_samples_per_class
    while True: #starts the generator
        #selects n_samples_per_class samples randomly from benign files and extracts features. replace=False means that the same item cannot be selected twice.
        ben_features = [
            extract_features(sha, path_to_benign_files, hash_dim=features_length)
            for sha in np.random.choice(benign_files, n_samples_per_class, replace=False)
        ]
        #selects n samples randomly from malicious files and extracts features
        mal_features = [
            extract_features(sha, path_to_malicious_files, hash_dim=features_length)
            for sha in np.random.choice(malicious_files, n_samples_per_class, replace=False)
        ]
        all_features = ben_features + mal_features #joins the two arrays of features
        labels = [0 for i in range(n_samples_per_class)] + [1 for i in range(n_samples_per_class)] #creates an array that is 2*n_samples_per_class long: the first half is made by 0s (benign samples) and the second half is made by 1s (malicious samples). The indexes match the ones in the "all_features" array.
        idx = np.random.choice(range(batch_size), batch_size) #generates a randomized index array -- BUG? NO REPLACE=FALSE
        all_features = np.array([np.array(all_features[i]) for i in idx]) #we shuffle the features for each sample, using the randomized array generated above
        labels = np.array([labels[i] for i in idx]) #we need to do the same thing for the labels
        yield all_features, labels #when prompted by our program (i.e. when the next() method is called), releases the current batch
