import numpy as np
import murmur
import re
import os

def read_file(sha, dir): #returns the contents of the file at the path dir/sha
    with open(os.path.join(dir, sha), 'r') as fp:
       file = fp.read()
    return file

def extract_features(sha, dir, hash_dim=1024, split_regex=r"\s+"): #the default regular expression "\s+" returns every sequence of consecutive characters between whitespaces, i.e. every word, HTML tag, etc.
    file = read_file(sha, dir)
    tokens = re.split(split_regex, file) #gets every word within the file
    token_hash_buckets = [ (murmur.string_hash(w) % (hash_dim - 1) + 1) for w in tokens ] #murmur.string_hash() returns the hash in numerical form, and we calculate the modulo against hash_dim (1024 by default), then we add 1. All results are stored within this array, which contains numbers in the 1:hash_dim interval. Each different number is called a "bucket"
    token_bucket_counts = np.zeros(hash_dim) #we create an array of zeros, with dimension hash_dim: we will use this to count how many hits each bucket got
    buckets, counts = np.unique(token_hash_buckets, return_counts=True) #reads the token_hash_buckets variable and returns a matrix, which we split in two variables. For each bucket (stored in the variable buckets), we have the number of its occurrences (stored in the variable counts) 
    for bucket, count in zip(buckets, counts): #we take the index of the bucket in the token_bucket_counts array we initialized above and we store the number of its occurrences.
        token_bucket_counts[bucket] = count
    return np.array(token_bucket_counts)
