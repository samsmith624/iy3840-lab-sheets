#we initialize some useful variables
batch_size = 128
features_length = 1024
path_to_training_benign_files = "data/html/benign_files/training/" #you might need to edit this path, depending on where you place this script
path_to_training_malicious_files = "data/html/malicious_files/training/" #you might need to edit this path, depending on where you place this script
steps_per_epoch = 1000 #how many batches must be analyzed per each epoch. Usually this number should be equal to the dataset size
epochs = 10

train_benign_files = os.listdir(path_to_training_benign_files) #gets all file names in benign folder
train_malicious_files = os.listdir(path_to_training_malicious_files) #gets all file names in malicious folder

#let's create our generator
training_generator = my_generator(
    benign_files=train_benign_files,
    malicious_files=train_malicious_files,
    path_to_benign_files=path_to_training_benign_files,
    path_to_malicious_files=path_to_training_malicious_files,
    batch_size=batch_size,
    features_length=features_length
)

#finally, we train our model using the variables defined above
model.fit_generator(
    generator=training_generator,
    steps_per_epoch=steps_per_epoch,
    epochs=epochs
)
