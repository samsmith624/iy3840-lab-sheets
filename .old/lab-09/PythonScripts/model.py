from keras import layers 
from keras.models import Model

input = layers.Input(shape=(1024,), dtype='float32') #our input layer is an array containing 1024 floats. dtype stands for "data type"
middle = layers.Dense(units=512, activation='relu')(input) #creates a densely connected layer (i.e. every output from previous layer is sent to every neuron in this layer), consisting of 512 neurons with ReLU activation function, taking input from the input layer
output = layers.Dense(units=1, activation='sigmoid')(middle) #creates a densely connected layer consisting of 1 neuron with sigmoid activation function, taking input from the middle layer
model = Model(inputs=input, outputs=output) #we create the model: we only specify the input and output layers, since we have already connected the middle layer(s) with the previous instructions
model.compile(optimizer='adam', #specifies backpropagation (training) algorithm
            loss='binary_crossentropy', #specifies loss function, i.e. the function to minimize during training
            metrics=['accuracy']) #specifies parameters to report during and after training
model.summary() #prints a summary of the layers
