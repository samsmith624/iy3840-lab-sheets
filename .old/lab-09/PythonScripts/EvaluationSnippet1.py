path_to_validation_benign_files = "data/html/benign_files/validation/" #edit this depending on where you are saving this script
path_to_validation_malicious_files = "data/html/malicious_files/validation/" #edit this depending on where you are saving this script
#we get all validation samples
val_benign_file_keys = os.listdir(path_to_validation_benign_files)
val_malicious_file_keys = os.listdir(path_to_validation_malicious_files)
#we reuse our generator to get us some validation data instead, this time. Notice the increase batch_size
validation_data = my_generator(
       benign_files=val_benign_file_keys,
       malicious_files=val_malicious_file_keys,
       path_to_benign_files=path_to_validation_benign_files,
       path_to_malicious_files=path_to_validation_malicious_files,
       batch_size=10000,
       features_length=features_length
).next() #we manually call this just once
