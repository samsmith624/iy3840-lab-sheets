#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
    printf("Allocating 8 bytes...\n");
    char buf[8]; //Allocates an 8 byte buffer
    if(argc != 2) { //Checks if the argument count is equal to 2 (which are the file name and the string to be printed)
        printf("Usage: %s <string>\n", argv[0]); //If it's not equal to 2, print the correct usage of this executable
        return 1; //Returns 1 instead of 0 because the program did not terminate correctly
    }
    strcpy(buf, argv[1]); //Writes the given string into the buffer
    printf("%s\n", buf); //Prints the given string
    return 0; //The program terminates correctly
}
