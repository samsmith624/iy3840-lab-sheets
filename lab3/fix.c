#define _GNU_SOURCE //Necessary to include dlfcn.h
#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>
#include  <stdint.h>
#include  <dlfcn.h> //Library for dynamic linking, we use it to get pointers to shared library functions

char* (*orig_strcpy)(char*, const char*); //This will store the pointer to the original strcpy function

char* strcpy(char *dst, const char *src) { //We define the new (patched) strcpy function
    if(!orig_strcpy) orig_strcpy = dlsym(RTLD_NEXT, "strcpy"); //If orig_strcpy has not been initialized, get the pointer to the original strcpy function and store it in orig_strcpy
    if(strlen(src) > 8) { //We just check if the length of the string to write is greater than the buffer we allocated in the original code
        printf("Bad idea! Aborting strcpy to prevent overflow!\n"); //If it is, we print an error message and exit the program to avoid the vulnerability to be exploited
        exit(1);
    } 
    return orig_strcpy(dst, src); //The original strcpy is executed only if the length of the string is less or equal to 8
}
