#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void normalFunc();
void evilFunc();

int main(int argc, char **argv) {
    char *secret = "12349876";
    int secret_known = 0;
    if (argc > 1)
       secret_known = (strcmp(argv[1], secret) == 0); //compares the input with the secret
    
    if (secret_known) //if they match, start malicious behaviour
        evilFunc();
    else //if they don't, pretend to be a normal hello world program
        normalFunc();
    return 0;
}

void normalFunc() {
  printf("Hello, world!\n");
}

void evilFunc() {
  printf("This string is nasty!\n");
}
