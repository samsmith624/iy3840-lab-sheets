
#!/usr/bin/python

import argparse
import os
import networkx
from networkx.drawing.nx_pydot import write_dot
import itertools

def jaccard(set1, set2):
    """
    Compute the Jaccard Distance between two sets by taking
    their intersection, union and then dividing the number
    of elements in the intersection by the number of elements
    in their union.
    """
    intersection = set1.intersection(set2) #calculates intersection between set1 and set2
    intersection_length = float(len(intersection)) #gets how many items are in the intersection
    union = set1.union(set2) #calculates union between set1 and set2
    union_length = float(len(union)) #gets how many items are in the union
    return intersection_length / union_length

def getstrings(fullpath):
    """
    Extract strings from the binary indicated by the 'fullpath'
    parameter, and then return the set of unique strings in
    the binary.
    """
    strings = os.popen("strings '{0}'".format(fullpath)).read() #executes the strings command, that extracts "human readable" characters from a file. Each string found is separated with a newline (\n)
    strings = set(strings.split("\n")) #the strings variable becomes an array. We split the strings on the newline character.
    return strings

def pecheck(fullpath):
    """
    Do a cursory sanity check to make sure 'fullpath' is
    a Windows PE executable (PE executables start with the
    two bytes 'MZ')
    """
    return open(fullpath, 'rb').read(2) == b'MZ' #returns true or false

if __name__ == "__main__": #returns true only if run as a script, so this won't run if we decide to import the previously defined functions within another program
    parser = argparse.ArgumentParser(description="Identify similarities between malware samples and build similarity graph")
    parser.add_argument("-target_directory", help="Directory containing malware")
    parser.add_argument("-output_dot_file", help="Where to save the output graph DOT file")
    parser.add_argument("-jaccard_index_threshold", "-j", dest="threshold", type=float, default=0.8, help="Threshold above which to create an 'edge' between samples")
    args = parser.parse_args() #parses arguments into the args variable

    malware_paths = [] #variable where we'll store the malware file paths
    malware_features = dict() #variable where we'll store the malware strings
    graph = networkx.Graph() #similarity graph

    for root, dirs, paths in os.walk(args.target_directory): #walk() returns all file names in a given directory tree
        #Walk the target directory tree and store all of the file paths
        for path in paths:
            full_path = os.path.join(root, path)
            malware_paths.append(full_path)

    # Filter out any paths that aren't PE files
    malware_paths_filter = filter(pecheck, malware_paths) #we defined pecheck() earlier

    # Get and store the strings for all of the malware PE files
    for path in malware_paths_filter:
        features = getstrings(path) #we defined getstrings() earlier as well
        print(f"Extracted {len(features)} features from {path} ...")
        malware_features[path] = features #since getstrings() returns an array of strings, malware_features will be a matrix
        graph.add_node(path, label=os.path.split(path)[-1][:10]) #add each malware file to the graph as a new node

    # Reset the iterator
    malware_paths_filter = filter(pecheck, malware_paths)

    # Iterate through all pairs of malware
    for malware1, malware2 in itertools.combinations(malware_paths_filter, 2): #returns all combinations of 2 elements from malware_paths. Uniqueness of elements is given by their position: hence, the same combination of the same elements is not repeated, assuming there are no duplicates within the array.
        jaccard_index = jaccard(malware_features[malware1], malware_features[malware2]) #computes the Jaccard Distance for the current pair

        # If the Jaccard Distance is above the threshold, add an edge that links them
        if jaccard_index > args.threshold:
            print(malware1, malware2, jaccard_index)
            graph.add_edge(malware1, malware2, penwidth=1+(jaccard_index-args.threshold)*10)

    # Write the graph to disk so we can visualize it later using fdp, sfpd or neato
    write_dot(graph, args.output_dot_file)
