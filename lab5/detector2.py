#!/usr/bin/python

# Importing stuff we need
import os
import sys
import pickle
import argparse
import re
import numpy
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction import FeatureHasher


# We define a function that will extract the strings by using regular expressions
def get_string_features(path, hasher):
    chars = b" -~" # " -~" is the set of characters that goes from %20 (whitespace) to %7E (tilde).
    min_length = 5
    string_regexp = b'[%s]{%d,}' % (chars, min_length)  # this regex will match every sequence of characters included in the set above, that is at least 5 chars long
    file_object = open(path, 'rb')
    data = file_object.read()
    pattern = re.compile(string_regexp)  # compile() transforms the regex string in objects that can be used for pattern matching
    strings = pattern.findall(data)  # returns an array that includes all strings that match our regex

    # We want to store string features in dictionary form...
    string_features = {}
    for string in strings:
        string_features[string] = 1  # ...so for each string found, an entry in string_features is created. For example, if we had a "foobar" string, we would have that string_features[foobar]=1.
    hashed_features = hasher.transform([string_features])  # hashing trick
    # Unfortunately, features are returned as a list of arrays, and in a sparse format nonetheless.
    # We then need to get them back into a dense numpy vector.
    hashed_features = hashed_features.todense()  # returns a dense matrix
    hashed_features = numpy.asarray(hashed_features)  # returns a numpy vector
    hashed_features = hashed_features[0]  # returns the actual feature vector
    # The following lines print some info and return the array
    print(f"Extracted {len(string_features)} strings from {path}")
    return hashed_features


# We still need to train our detector! But first, let's write a function to build our training model.
def get_training_data(benign_path, malicious_path, hasher):
    # This function will help us get absolute file paths for a given directory
    def get_training_paths(directory):
        targets = []
        for path in os.listdir(directory):
            targets.append(os.path.join(directory, path))
        return targets

    malicious_paths = get_training_paths(malicious_path)  # gets absolute file paths for malicious training samples
    benign_paths = get_training_paths(benign_path)  # gets absolute file paths for benign training samples
    X = [
        get_string_features(path, hasher) for path in malicious_paths + benign_paths
    ]  # do you recall get_string_features()? Yes, we defined it earlier! For each malicious samples, it gets all string features that match our criteria
    y = (
            [1 for i in range(len(malicious_paths))] + [0 for i in range(len(benign_paths))]
    )  # if the sample was labeled as malicious, assign 1, otherwise 0. Remember that we already know if these samples are malicious or not. This is, in other words, our ground truth.
    return X, y  # returns our training model


# We can now train our detector using the training model we generated!
def train_detector(X, y, hasher):
    classifier = RandomForestClassifier()  # sets the classifier as a decision tree that improves predictive accuracy and controls over-fitting
    classifier.fit(X, y)  # trains the detector
    pickle.dump((classifier, hasher), open("saved_detector.pkl", "wb+"))  # we save the detector in the saved_detector.pkl file, for future use


# With our new detector, we can try to detect malware in new unknown binaries. Let's write a function that does it.
def scan_file(path):
    if not os.path.exists("saved_detector.pkl"):  # we look for our saved detector, and exit if we don't find it
        print("Train a detector before scanning files.")
        sys.exit(1)
    with open("saved_detector.pkl", "rb") as saved_detector:
        classifier, hasher = pickle.load(saved_detector)  # if the file is found, classifier and hasher are loaded from the pickle file
    features = get_string_features(path, hasher)  # we extract string features from the unknown binary
    result_proba = classifier.predict_proba([features])[0][1]  # the classifier tries to infer whether the binary is malicious or not, given the extracted features. The second value of the first element (i.e. [0][1]) from the returned array corresponds to such probability
    if result_proba > 0.5:
        print(f"It appears this file is malicious! Estimated accuracy: {str(result_proba * 100)}%. Original result_proba value: {str(result_proba)}")
    else:
        print(f"It appears this file is benign. Estimated accuracy: {str((1 - result_proba) * 100)}%. Original result_proba value: {str(result_proba)}")


# We defined all functions we need, however we still have to write our core program that calls them.
parser = argparse.ArgumentParser("get windows object vectors for files")
# Our python program will ask for three parameters upon execution (see "help=" field):
parser.add_argument("--malware_paths", default=None, help="Path to malware training files")
parser.add_argument("--benignware_paths", default=None, help="Path to benignware training files")
parser.add_argument("--scan_file_path", default=None, help="File to scan")
args = parser.parse_args()  # parses arguments into the args variable
hasher = FeatureHasher(20000)  # our hasher will include 20000 hashed features when performing the hashing trick
if args.malware_paths and args.benignware_paths:  # checks if paths for both malware and benignware training samples have been specified
    X, y = get_training_data(args.benignware_paths, args.malware_paths, hasher)
    train_detector(X, y, hasher)
elif args.scan_file_path:  # if benignware and malware paths have NOT been specified, checks if path for the unknown binary has been specified
    scan_file(args.scan_file_path)
else:
    print("[*] You did not specify a path to scan, nor did you specify paths to malicious and benign training files.")
    print("Please specify one of these to use the detector.\n")
    parser.print_help()
